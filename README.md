# WAES Assignment for QA specialists

Thank you for your interest in WAES and for taking the time to complete QA challenge.
We love to push techology further to change life for the better. 
And quality assurance is important part of this changes.

## Goal

The goal of this assignment is to check your skills in Web UI testing.
We evaluate the assignment depending on your level of seniority regarding coverage, strategy and presentation.

## Task

Just imagine you work on new platform which is needed to be tested.
Your tasks are:

1. Create automation framework from the scratch for the following website: [https://waesworks.bitbucket.io](https://waesworks.bitbucket.io)
2. Automate next test suites:

	- Sign up
	- Login with validation of every role's super power

## Additional requirements

- Your project should cover as many functional scenarios as you judge necessary for the given features and the test data available.
- Your project should cover field validation.
- The tests should only check UI-based functionality (no need to validate page scripts as this is a static website).

## Technologies

The assignment can be developed with any of three most popular languages Java/JS/Python and any testing framework.
Choose your weapon of choice.

## Submission Guidelines

- Cover functional scenarios, and don't forget about some corner cases.
- Report with test results should be automatically generated.
- Include a `README.md` with instructions how to run tests, together with any additional information you consider appropriate (assumptions, design decisions made, etc.)

## Nice to have

- Toggle for on/off headless execution for the front-end tests.
- If defects are found, a report with all relevant information.
- Suggestions for improvements.



#### Please upload the assignment to your personal GitHub/Bitbucket account once finished and send the link to the responsible person in WAES before deadline.